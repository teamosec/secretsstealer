# **SecretsStealer** #
 
For now this is a knowledge conservation project only.
this repository contains a collection of tools and information can help gathering hidden information from an ios device.

## Keychain-dumper ##

dumps the keychain database into textual format, Keychain database is encrypted with a hardware specific key which is unique per the device. Hardware key cannot be extracted from the device, so the data stored in the keychain can only be accessible on the device itself and cannot be moved to another device. Keychain database is tied to the device, so even if an attacker obtains access to the keychain through a physical access to the file system or in a remote attack, he cannot decrypt and view the file contents.
And that is why the Keychain-dumper binary needs to be transferred and executed on the relevant device.


## BinaryCookieReader.py ##

Parses binary cookies into textual format.

## Kechain database ##

Keychain is an encrypted container (128 bit AES algorithm) and a centralized Sqlite database that holds identities & passwords for multiple applications and network services.
the database can be used only when on the device (as said before).

## Cookies ##

Cookies are mostly saved in the application bundle directory tree */var/mobile/Application/* under what seems like a serial number, each folder represents an application.
To find the relevant folder we need to look for the application binary name (ex. safari = MobileSafari) recursively  in these folders.
Inside the application directory in the Library/cookies folder is where cookies are saved in binarycookies format and we can use the *BinaryCookieReader.py* script to convert it to textual.

------------------------------------

### iCloud/Apple account 

The username and password of the current user are saved in the keychain database and can be used for
stealing personal information as photos, videos, contacts, calender, messages and more..

### WiFi ###

All wifi networks that the dvice was connected to ever are saved as ssids and passwords in the keychain database.

### Mails ###

Mails from apples mail application are all saved in the <> directory inside the folder named with the users mail address, in it we can find mails in HTML format and attachments.
Also the mails passwords are all saved in the keychain database.




 
*more information comming soon*